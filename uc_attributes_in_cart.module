<?php

/**
 * @file
 * Alters the tapir table of the Ubercart cart.
 * Show attribute's forms of products in the cart view form.
 */

/**
 * Implementation of hook_tapir_table_alter().
 */
function uc_attributes_in_cart_tapir_table_alter($table, $table_id) {
	// Modify the display of the cart table.
	if ($table_id == 'uc_cart_view_table') {
		// New column in the cart table.
		$table['#columns']['attributes_in_cart'] = array(
			'cell' => t('Attributes'),
			'weight' => 3
		);

		// Step on the right for these two columns.
		$table['#columns']['qty']['weight'] = 4;
		$table['#columns']['total']['weight'] = 5;

		// Set an area to hold the attribute(s) form(s).
		// First, we get the items in the current cart.
		$items = uc_cart_get_contents();

		for ($i = 0; $i < count($items); $i++) {
			// Set an empty area.
			$table[$i]['attributes_in_cart'] = NULL;
		}
	}
}

/**
 * Implementation of hook_form_alter().
 */
function uc_attributes_in_cart_form_alter(&$form, &$form_state, $form_id) {
	if ($form_id == 'uc_cart_view_form') {
		// Display attributes forms.
		// First, get the items in the current cart.
		$items = uc_cart_get_contents();

		for ($i = 0; $i < count($items); $i++) {
			// Get the options of the attributes of the product.
			$options = _uc_cart_product_get_options($items[$i]);

			//Load the product.
			$product = node_load($form['items'][$i]['nid']['#value']);

			//Check if the current user has access to the product.
			if (node_access('view', $product)) {
				// Display the attributes' forms.
				$form['items'][$i]['attributes_in_cart'] = _uc_attribute_alter_form($product);

				// Set the default value of the attributes.
				foreach ($options as $option) {
					// Check in the database that the user wants this attribute to be displayed.
					$display_attribute_settings = db_fetch_array(db_query_range("SELECT show_form, show_title FROM {uc_attributes_in_cart} WHERE aid = '%d'", $option['aid'], 0, 1));

					// Show the attribute form.
					if (!isset($display_attribute_settings['show_form']) || $display_attribute_settings['show_form']) {
						// Set the default values.
						$form['items'][$i]['attributes_in_cart'][$option['aid']]['#default_value'] = $items[$i]->data['attributes'][$option['aid']];

						// Do not display titles (by default they are displayed).
						if (isset($display_attribute_settings['show_title']) && !$display_attribute_settings['show_title']) {
							$form['items'][$i]['attributes_in_cart'][$option['aid']]['#title'] = '';
						}
					}

					// Do not show the attribute form.
					else {
						$form['items'][$i]['attributes_in_cart'][$option['aid']]['#access'] = FALSE;
					}
				}
			}
		}

		// Make the 'update cart' and 'checkout' buttons process our submit function.
		$form['#submit'][] = 'uc_attributes_in_cart_cart_view_form_submit';
		$form['update']['#submit'][] = 'uc_attributes_in_cart_cart_view_form_submit';
		$form['checkout']['#submit'][] = 'uc_attributes_in_cart_cart_view_form_submit';
	}
}

/**
 * Save the new attributes values modified by the user.
 */
function uc_attributes_in_cart_cart_view_form_submit($form, &$form_state) {
	//Get the items in the current cart.
	$items = uc_cart_get_contents();

	for ($i = 0; $i < count($items); $i++) {
		// Update the value of the attributes for each items.
		foreach ($items[$i]->data['attributes'] as $aid => $option) {
			$items[$i]->data['attributes'][$aid] = $form_state['values']['items'][$i]['attributes_in_cart'][$aid];

			// Add useless data to the item to avoid same products to be treated as one.
			$items[$i]->data['uc_attributes_in_cart'] = $i;
		}

		uc_cart_update_item($items[$i]);
	}
}

/**
 * Implementation of hook_menu().
 */
function uc_attributes_in_cart_menu() {
	$items = array();

	// Administrative page of this module.
	$items['admin/store/settings/attributes/in-cart'] = array(
		'title' => 'Attributes in cart settings',
		'description' => 'Administer the settings of attributes in cart.',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('uc_attributes_in_cart_settings'),
		'access arguments' => array('administer attributes in cart settings')
	);

	return $items;
}

/**
 * Administrative page of this module.
 */
function uc_attributes_in_cart_settings() {
	// Get the list of all attributes.
	$attributes = db_query("SELECT ua.aid, ua.name, ua.label, uaic.show_form, uaic.show_title FROM {uc_attributes} ua LEFT JOIN {uc_attributes_in_cart} uaic ON ua.aid = uaic.aid");

	// Main fieldset.
	$form['list'] = array(
		'#type' => 'fieldset',
		'#title' => t('Attributes'),
		'#description' => t('Here are listed the attributes you have defined in your store administration.')
	);

	// For each attribute, display options.
	while ($attribute = db_fetch_array($attributes)) {
		$form['list']['attribute_' . $attribute['aid']] = array(
			'#type' => 'fieldset',
			'#title' => empty($attribute['label']) ? $attribute['name'] : $attribute['name'],
			'#collapsible' => TRUE,
			'#collapsed' => TRUE
		);

		$form['list']['attribute_' . $attribute['aid']]['attribute_' . $attribute['aid'] . '_show_form'] = array(
			'#type' => 'checkbox',
			'#title' => t('Show attribute form in cart'),
			'#description' => t('Check this box if you want this attribute form to be displayed in cart.'),
			'#default_value' => !isset($attribute['show_form']) ? 1 : $attribute['show_form']
		);

		$form['list']['attribute_' . $attribute['aid']]['attribute_' . $attribute['aid'] . '_show_title'] = array(
			'#type' => 'checkbox',
			'#title' => t('Show attribute title in cart'),
			'#description' => t('Check this box if you want this attribute form to have a title in cart.'),
			'#default_value' => !isset($attribute['show_title']) ? 1 : $attribute['show_title']
		);
	}

	$form['submit'] = array('#type' => 'submit', '#value' => t('Save'));

	return $form;
}

/**
 * Save the user settings for this module.
 */
function uc_attributes_in_cart_settings_submit($form, &$form_state) {
	// INSERT/UPDATE the settings in the database according to the user settings.
	foreach ($form_state['values'] as $key => $value) {
		$label = explode('_', $key);

		// Check that it is a checkbox value.
		if ($label[0] == 'attribute' && is_numeric($label[1])) {
			// $label[1] is the attribute id (aid).
			$aid = $label[1];

			// $label[2] . '_' . $label[3] is the database column name where to insert the value.
			$column = $label[2] . '_' . $label[3];

			// Check if there is already a record in the database for this attribute.
			$test = db_result(db_query_range("SELECT aid FROM {uc_attributes_in_cart} WHERE aid = '%d'", $aid, 0, 1));

			// There is already a record for this attribute, UPDATE the database.
			if ($test) {
				db_query("UPDATE {uc_attributes_in_cart} SET %s = '%d' WHERE aid = '%d'", $column, $value, $aid);
			}

			// There is no record for this attribute, INSERT in the database.
			else {
				db_query("INSERT INTO {uc_attributes_in_cart} (aid, '%s') VALUES ('%d', '%d')", $column, $aid, $value);
			}
		}
	}

	// Display a confirmation message to the user.
	drupal_set_message(t('The settings have been saved.'));
}

/**
 * Implementation of hook_perm().
 */
function uc_attributes_in_cart_perm() {
	return array('administer attributes in cart settings');
}
