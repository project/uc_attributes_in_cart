$Id:

INSTALLATION

1 - Extract the module in the /modules directory of your choice (e.g. /sites/all/modules).
2 - Install the module (admin/build/modules).
3 - The module is installed and working. By default, all attributes of a product are shown in the cart along with their titles.



USAGE

1 - If you have not defined any attribute, go to admin/store/attributes/add and add one.
2 - Add an attribute to a product by going to node/%/edit/attributes/add (replace % with a number).
3 - At this point of the process, all attributes of a product are shown in the cart page.
4 - If you want not to show some of the attributes in the cart page, you can go at the administer page of the module at: admin/store/settings/attributes/in-cart
5 - Select the attributes you want to appear in the cart page and choose whether or not to display their titles.